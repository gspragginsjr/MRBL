import Vue from 'vue';

import App from '@/components/App';
import intersect from '@/js/directives/intersect';
import VueFire from 'vuefire';
import axios from 'axios';
import VueAxios from 'vue-axios';

// Modal
import ModalButton from '@/components/modal/ModalButton';
import ModalBackdrop from '@/components/modal/ModalBackdrop';
import ModalBackdropDialog from '@/components/modal/ModalBackdropDialog';

// Disclosure
import DisclosureButton from '@/components/disclosure/DisclosureButton';
import DisclosureContainer from '@/components/disclosure/DisclosureContainer';

// Accordion
import AccordionContainer from '@/components/accordion/AccordionContainer';
import AccordionContainerPanel from '@/components/accordion/AccordionContainerPanel';
import AccordionContainerHeader from '@/components/accordion/AccordionContainerHeader';
import AccordionContainerHeaderButton from '@/components/accordion/AccordionContainerHeaderButton';

// Tabs
import TabsContainer from '@/components/tabs/TabsContainer';
import TabsContainerTablist from '@/components/tabs/TabsContainerTablist';
import TabsContainerTabpanel from '@/components/tabs/TabsContainerTabpanel';
import TabsContainerTablistTab from '@/components/tabs/TabsContainerTablistTab';

// Filter
import GlobalFilter from '@/components/filter/GlobalFilter';
import GlobalFilterButton from '@/components/filter/GlobalFilterButton';
import GlobalFilterItemsWrap from '@/components/filter/GlobalFilterItemsWrap';
import GlobalFilterItem from '@/components/filter/GlobalFilterItem';
import GlobalFilterItemGhost from '@/components/filter/GlobalFilterItemGhost';

// Logo
import GlobalLogo from '@/components/GlobalLogo';

// MRBL Icon
import GlobalMrblIcon from '@/components/GlobalMrblIcon';

// Carousel 2
import GlobalCarousel from '@/components/carousel2/GlobalCarousel';
import GlobalCarouselDot from '@/components/carousel2/GlobalCarouselDot';

// Video
import GlobalVideo from '@/components/GlobalVideo';

// Tags
import GlobalTags from '@/components/GlobalTags';

// Four Block Layout
import GlobalFourBlockLayout from '@/components/GlobalFourBlockLayout';

// Purchase Form
import GlobalPurchaseForm from '@/components/GlobalPurchaseForm';

// Contribution Form
import GlobalContributionForm from '@/components/GlobalContributionForm';

// Progress Hexagon
import GlobalProgressHexagon from '@/components/GlobalProgressHexagon';

// Fixed Sidebar
import GlobalFixedSidebar from '@/components/GlobalFixedSidebar';

// Project Components
import ProjectHero from '@/components/ProjectHero';
import ProjectIncentiveBox from '@/components/ProjectIncentiveBox';

Vue.config.productionTip = false;

Vue.use(VueFire, VueAxios, axios);

Vue.directive('intersect', intersect);

// Modal
Vue.component('ModalButton', ModalButton);
Vue.component('ModalBackdrop', ModalBackdrop);
Vue.component('ModalBackdropDialog', ModalBackdropDialog);

// Disclosure
Vue.component('DisclosureButton', DisclosureButton);
Vue.component('DisclosureContainer', DisclosureContainer);

// Accordion
Vue.component('AccordionContainer', AccordionContainer);
Vue.component('AccordionContainerPanel', AccordionContainerPanel);
Vue.component('AccordionContainerHeader', AccordionContainerHeader);
Vue.component('AccordionContainerHeaderButton', AccordionContainerHeaderButton);

// Tabs
Vue.component('TabsContainer', TabsContainer);
Vue.component('TabsContainerTablist', TabsContainerTablist);
Vue.component('TabsContainerTabpanel', TabsContainerTabpanel);
Vue.component('TabsContainerTablistTab', TabsContainerTablistTab);

// Filter
Vue.component('GlobalFilter', GlobalFilter);
Vue.component('GlobalFilterButton', GlobalFilterButton);
Vue.component('GlobalFilterItemsWrap', GlobalFilterItemsWrap);
Vue.component('GlobalFilterItem', GlobalFilterItem);
Vue.component('GlobalFilterItemGhost', GlobalFilterItemGhost);

// Logo
Vue.component('GlobalLogo', GlobalLogo);

// Logo
Vue.component('GlobalMrblIcon', GlobalMrblIcon);

// Carousel 2
Vue.component('GlobalCarousel', GlobalCarousel);
Vue.component('GlobalCarouselDot', GlobalCarouselDot);

// Video
Vue.component('GlobalVideo', GlobalVideo);

// Tags
Vue.component('GlobalTags', GlobalTags);

// Four Block Layout
Vue.component('GlobalFourBlockLayout', GlobalFourBlockLayout);

// Purchase Form
Vue.component('GlobalPurchaseForm', GlobalPurchaseForm);

// Contribution Form
Vue.component('GlobalContributionForm', GlobalContributionForm);

// Progress Hexagon
Vue.component('GlobalProgressHexagon', GlobalProgressHexagon);

// Fixed Sidebar
Vue.component('GlobalFixedSidebar', GlobalFixedSidebar);

// Project Components
Vue.component('ProjectHero', ProjectHero);
Vue.component('ProjectIncentiveBox', ProjectIncentiveBox);

/* eslint-disable */
new Vue(App).$mount('#app');

var tag = document.createElement('script');
tag.src = "//www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var looseyScript = document.createElement('script');
looseyScript.appendChild(document.createTextNode('var videoList = []; var readyYoutubeVids = [];'));
firstScriptTag.parentNode.insertBefore(looseyScript, firstScriptTag);

window.onYouTubePlayerAPIReady = function() {

    window.addEventListener("load", function() {

        for (var video in window.videoList) {
            window.readyYoutubeVids.push(new YT.Player(window.videoList[video]));
        }
    });
}
/* eslint-enable */
