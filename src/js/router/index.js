import Vue from 'vue';
import VueRouter from 'vue-router';

import HomePage from '@/components/HomePage';
import ProjectPage from '@/components/ProjectPage';
import ProjectIndexPage from '@/components/ProjectIndexPage';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            name: 'home',
            path: '/',
            component: HomePage,
        },
        {
            name: 'project',
            path: '/project',
            component: ProjectPage,
        },
        {
            name: 'projects',
            path: '/projects',
            component: ProjectIndexPage,
        },
    ],
});
