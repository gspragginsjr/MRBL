const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const OSTSDK = require('@ostdotcom/ost-sdk-js');
const axios = require('axios');
// require SDK

const apiVault = {
    key: '759e4da5ccaae6d7f327',
    secret: '494a9b7f2e5b72fd6418f63d8ae2eaaea6c5b28d3fb11d54d19da5bb1cc2e5c1',
    endpoint: 'https://sandboxapi.ost.com/v1.1/',
};
const app = express();
const ostObj = new OSTSDK({
    apiKey: apiVault.key,
    apiSecret: apiVault.secret,
    apiEndpoint: apiVault.endpoint
});

app.use(cors());
app.use(bodyParser.json());

app.all('/airdrop', (req, res) => {
   if (res.statusCode === 200) {
        // do stuff
   }
});

app.all('/contribute', (req, res) => {
   if (res.statusCode === 200) {
        // do stuff
    }
});

app.all('/conversion', (req, res) => {
   if (res.statusCode === 200) {
        const clientResponse = res;
        const tokenService = ostObj.services.token;

        let currentOSTPrice;
        let conversionFactor;
        let price;

        tokenService.get({}).then(function(res) {
            currentOSTPrice = res.data.price_points.OST.USD;
            conversionFactor = res.data.token.conversion_factor;
            price = currentOSTPrice / conversionFactor;
            clientResponse.send(JSON.stringify(price));
        })
        .catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.all('/list-actions', (req, res) => {
   if (res.statusCode === 200) {
        const clientResponse = res;
        const actionService = ostObj.services.actions;

        actionService.list({})
        .then(function(res) {
            clientResponse.send(JSON.stringify(res.data));
        })
        .catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.get('/list-transactions/:id', (req, res) => {
   if (res.statusCode === 200) {
        const clientResponse = res;
        const ledgerService = ostObj.services.ledger;

        const id = req.params.id;

        ledgerService.get({
            id: id,
            limit: 10
        })
        .then(function(res) {
            clientResponse.send(JSON.stringify(res.data));
        }).catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.get('/purchase/:id/:amount', (req, res) => {
   if (res.statusCode === 200) {
        const clientResponse = res;
        const transactionService = ostObj.services.transactions;

        const id = req.params.id;
        const amount = req.params.amount;

        console.log(id);
        console.log(amount);

        transactionService.execute({
            from_user_id: '89bfe7bc-1065-4f43-9fe6-2188e7645438',
            to_user_id: id,
            action_id:'30703',
            amount: amount
        }).then(function(res) {
            clientResponse.send(JSON.stringify(res.data));
        })
        .catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.get('/user/:id', (req, res) => {
   if (res.statusCode === 200) {
        // do stuff
        const clientResponse = res;
        const userService = ostObj.services.users;
        const id = req.params.id;

        userService.get({id: id}).then(function(res) {
            clientResponse.send(JSON.stringify(res.data));
        })
        .catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.get('/user-name/:id', (req, res) => {
   if (res.statusCode === 200) {
        // do stuff
        const clientResponse = res;
        const userService = ostObj.services.users;
        const id = req.params.id;

        userService.get({id: id}).then(function(res) {
            clientResponse.send(JSON.stringify(res.data.user.name));
        })
        .catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.all('/users', (req, res) => {
   if (res.statusCode === 200) {
        const clientResponse = res;
        const userService = ostObj.services.users;

        userService.list({}).then(function(res) {
            // console.log(JSON.stringify(res));
            clientResponse.send(JSON.stringify(res));
        })
        .catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.all('/token', (req, res) => {
   if (res.statusCode === 200) {
        // do stuff
        const clientResponse = res;
        const tokenService = ostObj.services.token;
        tokenService.get({}).then(function(res) {
            clientResponse.send(JSON.stringify(res));
        })
        .catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.get('/transaction/:from/:to/:id', (req, res) => {
   if (res.statusCode === 200) {
        const clientResponse = res;
        const transactionService = ostObj.services.transactions;

        const from = req.params.from;
        const to = req.params.to;
        const id = req.params.id;

        transactionService.execute({
            from_user_id: from,
            to_user_id: to,
            action_id: id,
        }).then(function(res) {
            clientResponse.send(JSON.stringify(res.data));
        })
        .catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

app.get('/transaction-status/:id', (req, res) => {
   if (res.statusCode === 200) {
        const clientResponse = res;
        const transactionService = ostObj.services.transactions;

        const id = req.params.id;

        transactionService.get({
            id: id,
        }).then(function(res) {
            clientResponse.send(JSON.stringify(res.data));
        }).catch(function(err) {
            console.log(JSON.stringify(err));
        });
    }
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log('Also listening on http://localhost:3000');
    console.log(ostObj);
});
